# OpenML dataset: Case-Study-Applicants-for-a-Gold-Digger-position

https://www.openml.org/d/43552

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataframe describes applications for a Gold Digger position. According to each applicants's characteristics, can you create the best model to classify whether a candidate is hired or not ?
It is a good playground to harden your data science skills and try new models. Ideal to prepare interviews.
Content
This dataframe contains 20000 observations and 11 columns:

date: date of the application
age: age of the candidate
diplome: highest qualification diploma (bac, licence, master, doctorat)
specialite: minor of the diploma (geologie, forage, detective, archeologie,)
salaire: salary expectation
dispo: oui : directly available, non : not directly available
sexe: female (F) or male (M)
exp: years of relevant experience
cheveux: hair color (chatain, brun, blond, roux)
note: grade (out of 100) for gold digging exam
embauche: Has the candidate been hired ? (0 : no, 1 : yes)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43552) of an [OpenML dataset](https://www.openml.org/d/43552). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43552/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43552/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43552/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

